
CREATE OR REPLACE VIEW v_terms_taxonomy AS 
SELECT * FROM wp_terms 
INNER JOIN wp_term_taxonomy USING (term_id) 
ORDER BY taxonomy, `name`;
