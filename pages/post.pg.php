	<h1>Post: <?php echo_utf8($post->post_title); ?></h1>
	<h3>Post Type: <?php echo_utf8($post->post_type); ?></h3>

	<table class="ink-table alternating hover">
	    <tbody>
	<?php
	    foreach($post as $k => $v) {
		?><tr>
		    <td><?php echo $k; ?></td>
		    <td><?php

		    if ($k == 'ID') {
		    	echo $v . ' | ';
                ?><a href="<?=$wp_siteurl ?>/post.php?post=<?php echo $v; ?>&action=edit" target="_blank">backoffice</a><?php
            } elseif ($k == 'post_content') {
		    	if ($post->post_type == 'feedback') {
					$v = str_replace('<!--more-->', '', $v);

					echo "<pre>";
					echo jsonAsHTML($v);
					echo "</pre>";
		    	} else {
		    		echo_utf8($v);
		    	}
		    } else {
		    	echo_utf8($v);
		    }

		    ?></td>
		</tr>
		<?php
	    }
	?>
		</tbody>
	</table>

	<br>

	<h2>Taxonomies</h2>

	<table class="ink-table alternating hover">
	    <thead>
	    <tr>
			<th>taxonomy</th>
			<th>term name</th>
			<th>term slug</th>
	    </tr>
	    </thead>
	    <tbody>
	<?php
	    $sql = "SELECT * FROM v_terms_taxonomy";
	    $sql .= " WHERE term_taxonomy_id IN";
	    $sql .= " (SELECT term_taxonomy_id FROM wp_term_relationships WHERE object_id = " . $post->ID . ")";
	    $sql .= "";

	    $stmt_terms = $conn->query($sql);
	    $lista = $stmt_terms->fetchAll(); 
	    foreach($lista as $row) {
		?><tr>
		    <td><?php echo_utf8($row['taxonomy']); ?></td>
		    <td><?php echo_utf8($row['name']); ?></td>
		    <td><?php echo_utf8($row['slug']); ?></td>
		</tr><?php
	    }
	?>
		</tbody>
	</table>

	<h2>Metadata</h2>

	<table class="ink-table alternating hover">
	    <thead>
	    <tr>
			<th>key</th>
			<th>value</th>
	    </tr>
	    </thead>
	    <tbody>
	<?php
	    $lista_metadata = $conn->getPostMetadata($post->ID);
	    foreach($lista_metadata as $md) {
		?><tr>
		    <td><?php echo_utf8($md['meta_key']); ?></td>
		    <td><?php
		    
		    	
			    if ($md['meta_key'] == 'mediabank_album') {
			    	$maybe_object = unserialize(utf8_encode($md['meta_value']));
			    } else {
			    	$maybe_object = @unserialize(utf8_encode($md['meta_value']));
			    }
		    
		    	
		    	
		    	if ($maybe_object) {
		    		$type = gettype($maybe_object);
		    		
		    		if ($type != 'string') {
		    			echo $type . '<br>' . PHP_EOL;
		    		}
		    		
		    		if ($type == 'object') {
		    			arrayToList((array) $maybe_object);
		    		} elseif ($type == 'array') {
		    			arrayToList($maybe_object);
		    		} else {
		    			echo $type . ' :: ';
		    			echo_utf8($md['meta_value']);
		    		}
		    	} else {
					if ($md['meta_key'] == 'partner') {
						$partner = $conn->getPost($md['meta_value']);

						if ($partner) {
							echo $md['meta_value'] . ' | ';
							echo echo_utf8($partner->post_title);
							?> | <a href="/post.php?idp=<?php echo $partner->ID; ?>">info</a>
							
							<?php if (APP_KEY == 'lifestyle') { ?>
							 | 
							<a href="http://lifestyle.local/parceiros/<?php echo $partner->post_name; ?>" title="frontend">FE</a><?php
							}
						}
					} elseif ($md['meta_key'] == 'permalink') {
						 ?><a href="<?php echo($md['meta_value']); ?>"><?php echo_utf8($md['meta_value']); ?></a><?php
					} else {
						echo_utf8($md['meta_value']);
					}
		    	}
		    ?></td>
		</tr><?php
	    }
	?>
		</tbody>
	</table>
	
	<h2>Child Posts</h2>
	
	<?php
	$lista_childs = $conn->getChildPosts($post->ID);

	if ($lista_childs) {
		?>
		<table class="ink-table alternating hover">
		    <thead>
		    <tr>
				<th>title</th>
				<th>type</th>
		    </tr>
		    </thead>
		    <tbody>
			<?php
			    foreach($lista_childs as $row) {
				?><tr>
				    <td><a href="<?php echo $_SERVER['SCRIPT_NAME'] ?>?idp=<?php echo $row['ID']; ?>"><?php echo_utf8($row['post_title']); ?></a></td>
				    <td><?php echo_utf8($row['post_type']); ?></td>
				</tr>
				<?php
			    }
			?>
			</tbody>
		</table>
		<?php
	} else {
		echo "none";
	}
	?>

	<h2>Parent Post</h2>

	<?php
	$parent_post = $conn->getPost($post->post_parent);
	
	if ($parent_post) {
	    $link_txt = $parent_post->post_title . " (" . $parent_post->post_type . ")";
	    
	    ?><a href="<?php echo $_SERVER['SCRIPT_NAME'] ?>?idp=<?php echo $parent_post->ID; ?>"><?php echo_utf8($link_txt); ?></a><?php
	} else {
	    echo "none";
	}
