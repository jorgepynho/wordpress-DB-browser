<h1>Options</h1>

<table class="ink-table alternating hover">
    <thead>
    <tr>
		<th>id</th>
		<th>name</th>
		<th>value</th>
		<th>autoload</th>
    </tr>
    </thead>
    <tbody>
    <?php
	$list = $conn->getOptions(array("AND option_name NOT LIKE '%_transient_%'"));

    foreach($list as $row) {
		echo "<tr>" . PHP_EOL;
		echo "<td>" . $row['option_id'] . "</td>" . PHP_EOL;
		echo "<td>" . $row['option_name'] . "</td>" . PHP_EOL;
		echo "<td>";

        if ($row['option_name'] == 'cron') {
            ?><a href="/cron.php">go to cron page</a><td><?php
            continue;
        }

        $maybe_object = @unserialize(utf8_encode($row['option_value']));

		if ($maybe_object === false) {
			echo_utf8($row['option_value']);
		} else {
			$type = gettype($maybe_object);

			if ($type != 'string') {
				echo $type . '<br>' . PHP_EOL;
			}

			if ($type == 'object') {
				arrayToList((array) $maybe_object);
			} elseif ($type == 'array') {
				arrayToList($maybe_object);
			} else {
				echo $type . ' :: ';
				echo_utf8($row['option_value']);
			}
		}

		echo "&nbsp;</td>" . PHP_EOL;

		echo "<td>" . $row['autoload'] . "</td>" . PHP_EOL;
		echo "</tr>" . PHP_EOL;
	}
    ?>
    </tbody>
</table>
