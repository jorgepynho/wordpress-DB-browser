<h1>Posts: <?php echo $requested_post_type; ?></h1>

<nav class="ink-navigation">
<ul id="posts_types_filter" class="menu horizontal small">
    <li><a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>">*</a></li>
    <?php
	$posts_types = $conn->getPostTypes([' AND post_parent = 0']);
	foreach($posts_types as $pt) {
	    $ptu = utf8_encode($pt['post_type']);
	    ?><li><a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?pt=<?php echo $ptu; ?>"><?php echo $ptu; ?></a></li>
	    <?php
	}
    ?>
</ul>
</nav>

<span class="note">(revisions are ignored)</span><br><br>

<?php
    function listPosts($filter = '', $l = 0) {
	global $conn;
	
	$rs_posts = $conn->getPosts($filter);
	if ($rs_posts) {
	    foreach($rs_posts as $p) {
		
		?><ul class="posts_list">
		    <li><a href="post.php?idp=<?php echo $p['ID']; ?>"><?php echo_utf8($p['post_title']); ?></a>&nbsp;(<a href="posts.php?pt=<?php echo $p['post_type']; ?>"><?php echo $p['post_type']; ?></a>)
		    
		    <?php
		    unset($filter['post_type']);
		    $filter['post_parent'] = 'AND post_parent = ' . $p['ID'];
		    
		    listPosts($filter, $l + 1);
		    ?>
		    </li>
		</ul><?php
	    }
	} else {
	    echo "sem registos!";
	}
    }
    
    $filter = array();
    
    $filter['ignore_revisions'] = ' AND post_type <> \'revision\'';
    
    if ($requested_post_type) {
		$filter['post_type'] = "AND post_type = '$requested_post_type'";
    }
    
    $filter['post_parent'] = 'AND post_parent = 0';
    
    listPosts($filter);
    
