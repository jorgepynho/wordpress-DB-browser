<h1>All Term meta</h1>

<table class="ink-table alternating hover">
    <thead>
    <tr>
	<th>term</th>
	<th>meta</th>
	<th>value</th>
    </tr>
    </thead>
    <tbody>
<?php
    $sql = "SELECT * FROM wp_terms INNER JOIN wp_termmeta USING (term_id) ORDER BY `name`";
    $stmt = $conn->query($sql);
    $lista = $stmt->fetchAll();
    $tKey = '';
    foreach($lista as $row) {
	?><tr>
	    <td><?php
		if ($row['name'] != $tKey) {
		    echo_utf8($row['name']);
		    $tKey = $row['name'];
		}
	    ?></td>
	    <td><?php echo_utf8($row['meta_key']); ?></td>
	    <td><?php echo_utf8($row['meta_value']); ?></td>
	</tr>
	<?php
    }
?>
	</tbody>
</table>
