<h1>Wordpress Users</h1>

<table class="ink-table alternating hover">
    <thead>
    <tr>
		<th>id</th>
		<th>user_login</th>
		<th>user_nicename</th>
		<th>user_email</th>
		<th>user_url</th>
		<th>user_registered</th>
		<th>user_status</th>
		<th>display_name</th>
    </tr>
    </thead>
    <tbody>
    <?php
	$list = $conn->getUsers();
	
	foreach($list as $row) {

		$user_login = $row['user_login'];

		if (!$user_login) {
			$user_login = '<< vazio >>';
		}

		?><tr>
            <td><?=$row['ID']; ?></td>
            <td><a href="/user_meta.php?idu=<?=$row['ID']; ?>"><?=$user_login; ?></a></td>
            <td><?=$row['user_nicename']; ?></td>
            <td><?=$row['user_email']; ?></td>
            <td><?=$row['user_url']; ?></td>
            <td><?=$row['user_registered']; ?></td>
            <td><?=$row['user_status']; ?></td>
            <td><?=$row['display_name']; ?></td>
        </tr>
		<?php
	}
    ?>
    </tbody>
</table>
