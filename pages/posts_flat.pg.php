<h1>Posts (flat view)</h1>

<?php
    function listPosts($filter = array(), $l = 0) {
	global $conn;
	
	$rs_posts = $conn->getPosts($filter);
	if ($rs_posts) {
	    while($p = $rs_posts->fetch_object()) {
		
		?><ul class="posts_list">
		    <li><a href="post.php?idp=<?php echo $p->ID; ?>"><?php echo_utf8($p->post_title); ?></a>&nbsp;(<a href="posts.php?pt=<?php echo $p->post_type; ?>"><?php echo $p->post_type; ?></a>)
		    </li>
		</ul><?php
	    }
	} else {
	    echo "sem registos!";
	}
    }
    
    listPosts();
