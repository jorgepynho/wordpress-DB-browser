<?php
$term = $conn->getTerm($idt);

?><h1>Term: <?php echo_utf8($term->name); ?></h1><?php

?><h3>meta</h3><?php

$atts['class'] = 'ink-table alternating hover';

$sql = "SELECT * FROM wp_termmeta WHERE term_id = $idt";
select_to_table($conn, $sql, $atts);

?>
<br>
<h3>taxonomias</h3><?php

$list_txns = $conn->getTermTaxonomies($term->name);
foreach($list_txns as $tx) {
    echo $tx . "<br>" . PHP_EOL;
}
