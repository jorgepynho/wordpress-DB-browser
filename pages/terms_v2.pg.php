<h1>Terms</h1>
<?php
    //listChildTermsOfTerm();

    function listUniqueTerms($p = 0, $l = 0) {
	global $conn;
	
	?><ul><?php
	
	$lista = $conn->getUniqueTerms($p);
	foreach($lista as $row) {
	    ?><li><a href="<?php echo getScriptName(); ?>?idt=<?php echo $row['term_id'];?>"><?php echo_utf8($row['name']);?></a>
		<?php listUniqueTerms($row['term_id']); ?></li>
	    <?php
	}
	?></ul><?php
    }
    
    listUniqueTerms();
