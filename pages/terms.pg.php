<h1>Terms</h1>
<table class="ink-table alternating hover">
    <tr>
	    <!--th>id</th-->
	    <th>term</th>
	    <th>taxonomies</th>
	    <th>meta</th>
	</tr>
<?php
    //listChildTermsOfTerm();

    function listUniqueTerms($p = 0, $l = 0) {
	global $conn;
	
	$tmpl = getTemplateContent('row_term_unique_1');
	$flds = $conn->getTemplateFields($tmpl);
	
	$rs = $conn->getUniqueTerms($p);
	while($row = $rs->fetch_assoc()) {
	    
	    $list_txns = $conn->getTermTaxonomies($row['name']);
	    $str_txns = "(" . count($list_txns) . ") ";
	    $str_txns .= implode(', ', $list_txns);
	    
	    $term_meta = $conn->getTermMetaCSV($row['term_id']);
	    
	    $row['taxonomies'] = $str_txns;
	    $row['term_meta'] = $term_meta;
	    
	    $row['indent'] = '';
	    if ($l) {
		$row['indent'] = str_repeat('-', 4 * $l) . ' ';
	    }
	    
	    $conn->output_row($tmpl, $flds, $row);
	    
	    listUniqueTerms($row['term_id'], $l + 1);
	}
    }
    
    listUniqueTerms();
?>
</table>
