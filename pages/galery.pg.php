<h1>Gallery: <?php echo_utf8($post->post_title); ?></h1>

<h2>Fotos</h2>

<?php
    $sql = "SELECT *";
    $sql .= " FROM wp_posts WHERE ID IN (";
    $sql .= " SELECT meta_value FROM wp_postmeta WHERE post_id = " . $post->ID . " AND meta_key = 'photos')";
    $sql .= "";
    $sql .= "";

    $stmt_terms = $conn->query($sql);
    $lista = $stmt_terms->fetchAll(); 
    
    /*echo '<pre>';
    print_r($lista);
    echo '</pre>';*/

    ?><section id="galeria"><?php

    foreach($lista as $k => $post) {
    	?><img class="foto" src="<?php echo $post['guid']; ?>" alt="">
    	<?php
    }

    ?>

	</section>
	