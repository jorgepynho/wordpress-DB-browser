<h1>Cron</h1>
<hr>
<?php
	$cron = $conn->get_cron();

	echo count($cron);

	?> jobs - <a href="?a=clear_cron" onclick="return confirm('are you sure ?');">limpar jobs</a><hr>
	<br><?php

	foreach($cron as $k => $v) {
		echo $k . '<br>' . PHP_EOL;

		$type = gettype($v);

		if ($type == 'object') {
			echo '<pre>';
			$v = (array) $v;
			echo '</pre>';
		}

		if ($type == 'array') {
			
			$task_name = key($v);
			$task_data = current($v);
			
			$task_data_key = key($task_data);
			$task_data_params = current($task_data);

			echo '<strong>' . $task_name . '</strong><br>';
			echo $task_data_key . '<br><pre>';
			print_r($task_data_params);
			echo '</pre>';
		} elseif ($type == 'string') {
			echo $v;
		} elseif ($type == 'integer') {
			echo $v;
		} else {
			echo '<pre>';
			var_dump($v);
			echo '</pre>';
		}

		echo '<hr>' . PHP_EOL;
	}
