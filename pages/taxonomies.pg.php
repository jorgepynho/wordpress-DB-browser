<?php
function listChildTermsOfTerm($t = 0, $l = 0, $txn = '') {
    global $conn;

    $child_terms = $conn->getChildsOfTerm($t);
    if ($child_terms) {
	
	$tmpl = getTemplateContent('row_term_1');
	
	$use_fields = $conn->getTemplateFields($tmpl);
	
	foreach($child_terms as $row) {
	    
	    //print_r($row);
	    
	    $row['indent'] = str_repeat('-', $l * 4) . ' ';
	    
	    $conn->output_row($tmpl, $use_fields, $row);
	    
	    listChildTermsOfTerm($row['term_id'], $l + 1, $row['taxonomy']);
	}
    }
}

    $req_taxonomy = http_get('t');
?>
	<h1>Taxonomias</h1>
	<table class="ink-table alternating hover">
	    <thead>
	    <tr>
		<th>taxonomy</th>
		<th>term count</th>
	    </tr>
	    </thead>
	    <tbody>
	<?php
	    $lista = $conn->getTaxonomiesList();
	    if ($lista) {
			foreach($lista as $row) {
			    ?><tr <?php if ($row['taxonomy'] == $req_taxonomy) {echo " style=\"background-color: #DDDDDD\"";} ?>>
				<td><a href="<?php echo $_SERVER['SCRIPT_NAME'] ?>?t=<?php echo $row['taxonomy']; ?>"><?php echo $row['taxonomy']; ?></a><?php
				?></td>
				<td><?php echo $row['term_count']; ?></td>
			    </tr><?php
			    //listUniqueTaxonomies($t->term_taxonomy_id, $l+1);
			}
	    }
	    ?>
		    </tbody>
		</table>
	<?php
	if ($req_taxonomy) {
	    
	    $rs_txm_t = $conn->getTaxonomyTerms($req_taxonomy);
	    
	    ?><h2><?php echo_utf8($req_taxonomy); ?> - terms <span title="<?php echo count($rs_txm_t); ?> top level terms">(<?php echo count($rs_txm_t); ?>)</span></h2>
		<table class="ink-table alternating hover">
		    
		<?php
		
		    //$conn->listByTemplate('row_term_1', $rs_txm_t);
		
		    if ($rs_txm_t) {
			
			$tmpl = getTemplateContent('row_term_1');
			
			$use_fields = $conn->getTemplateFields($tmpl);
			
			foreach($rs_txm_t as $row) {
			    
			    $conn->output_row($tmpl, $use_fields, $row);
			    
			    listChildTermsOfTerm($row['term_id'], 1, $row['taxonomy']);
			}
		    }
	?>
	</table>
	<?php
	}
