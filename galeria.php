<?php
    require "_config.php";
    
    $idg = http_get('idg', 0);
    $slug = http_get('slug', '');

    $post = $conn->getPost($idg);

    if (!$post) {
        $post = $conn->getPostBySlug($slug);

        if (!$post) {
            echo("não tem post");
            exit;
        }
    }

    $winTitle = 'Galery: ' . $post->ID . ' ' . $post->post_name;
    
    $page = "galery";
    
    require $template_path;
    