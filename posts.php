<?php
    require "_config.php";
    
    $requested_post_type = http_get('pt');
    
    $winTitle = 'Posts | ' . $winTitle;

    if ($requested_post_type) {
    	$winTitle .= ': ' . $requested_post_type;
    }
    
    $view = http_get('v');
    
    if ($view == 'flat') {
		$page = "posts_flat";
    } else {
		$page = "posts";
    }
    
    require $template_path;
