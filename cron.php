<?php

    require "_config.php";

    $action = http_get('a');

    if ($action == 'clear_cron') {
        $conn->clear_cron();
    }

    $page = "crons/list";

    require $template_path;
