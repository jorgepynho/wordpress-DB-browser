<?php

class MPConn extends PDO {
    
    public function __construct($dsn, $u, $p, $opt = array()) {
    	
    	try {
    		parent::__construct($dsn, $u, $p, $opt);
    	} catch (PDOException $e) {
    		echo 'Connection failed: ' . $e->getMessage();
    	}
    	
		
    }
    
    public function getObject($sql) {

		$rs = $this->query($sql);
		
		if ($rs) {
			return (object) $rs->fetchAll()[0];
		}
		
		return false;
    }
    
    public function getSimpleArray($sql) {
		$rs = $this->query($sql);
		$list = array();
		while($row = $rs->fetch_row()) {
			$list[] = $row[0];
		}
		
		return $list;
    }
    
    public function getTaxonomiesList() {
		$sql = "SELECT COUNT(term_id) AS term_count, taxonomy";
		$sql .= " FROM v_terms_taxonomy";
		$sql .= " GROUP BY taxonomy";
		$sql .= " ORDER BY taxonomy";

		$stmt = $this->query($sql);

		return $stmt->fetchAll();
    }
    
    public function getPostTypes($filter = array()) {
		$sql = "SELECT DISTINCT post_type from wp_posts";
		
		if ($filter) {
			$sql .= " WHERE 1 = 1";
			foreach($filter as $f => $v) {
			$sql .= ' ' . $v;
			}
		}
		
		$sql .= " ORDER BY post_type";
		
		//echo $sql;
		
		$stmt = $this->query($sql);
		
		return $stmt->fetchAll();
    }
    
    public function getUsers($filter = array()) {
    	$sql = "SELECT * FROM wp_users";
    
    	if ($filter) {
    		$sql .= " WHERE 1 = 1";
    		foreach($filter as $f => $v) {
    			$sql .= ' ' . $v;
    		}
    	}
    
    	$sql .= " ORDER BY user_login";
    
    	//echo $sql;
    
    	$stmt = $this->query($sql);
    
    	return $stmt->fetchAll();
    }
    
    public function get_user_meta($id) {
        $sql = "SELECT * FROM wp_usermeta WHERE user_id = " . $id;

        $stmt = $this->query($sql);

        $rows = $stmt->fetchAll();

        return $rows;
    }

    public function getOptions($filter = array()) {
    	$sql = "SELECT * FROM wp_options";
    
    	if ($filter) {
    		$sql .= " WHERE 1 = 1";
    		foreach($filter as $f => $v) {
    			$sql .= ' ' . $v;
    		}
    	}
    	
    	$sql .= " ORDER BY option_name";

    	$stmt = $this->query($sql);

    	return $stmt->fetchAll();
    }

    public function get_wp_option($option, $def = '') {
        $sql = "SELECT option_value FROM wp_options WHERE option_name = '$option'";

        $stmt = $this->query($sql);

        $col = $stmt->fetchColumn(0);

        if ($col) return $col;

        return $def;
    }

    public function get_wp_siteurl() {
        return $this->get_wp_option('siteurl'); //wp backoffice URL
    }

    public function get_wp_home() {
        return $this->get_wp_option('home');
    }

    public function getPosts($filter) {
		$sql = "SELECT ID, post_title, post_type, post_name FROM wp_posts";

		if ($filter) {
		    $sql .= " WHERE 1 = 1";
		    foreach($filter as $f => $v) {
				$sql .= ' ' . $v;
		    }
		}

		$sql .= ' ORDER BY ID DESC';

		$stmt = $this->query($sql);
		
		return $stmt->fetchAll();
    }
    
    public function getPost($idp) {
		$sql = "SELECT * FROM wp_posts WHERE ID = $idp";

		$stmt = $this->query($sql);

		$lista = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($lista) {
		    return (object) $lista[0];
		}
		
		return false;
    }
    
    public function getPostBySlug($slug) {
		$sql = "SELECT * FROM wp_posts WHERE post_name = '$slug'";
		$stmt = $this->query($sql);
		
		$lista = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($lista) {
		    return (object) $lista[0];
		}
		
		return false;
    }

    public function getPostMetadata($idp) {
		$sql = "SELECT * FROM wp_postmeta WHERE post_id = $idp ORDER BY meta_key";
	
		//echo $sql;
	
		$stmt = $this->query($sql);
		
		return $stmt->fetchAll();
    }
    
    public function getChildPosts($idp) {
	$sql = "SELECT * FROM wp_posts WHERE post_parent = $idp ORDER BY ID DESC";
		$stmt = $this->query($sql);
		
		return $stmt->fetchAll();
    }
    
    public function getTaxonomyTerms($tx) {
	$sql = "SELECT term_taxonomy_id, term_id, `name`, taxonomy, slug, parent";
	$sql .= " FROM v_terms_taxonomy";
	$sql .= " WHERE taxonomy = '$tx'";
	$sql .= " ORDER BY `name`";
	
	//echo $sql;
		$stmt = $this->query($sql);
		
		return $stmt->fetchAll();
    }
    
    public function getChildsOfTerm($idTerm) {
	$sql = "SELECT * FROM v_terms_taxonomy WHERE parent = $idTerm ORDER BY name";
	//echo $sql;
		$stmt = $this->query($sql);
		
		return $stmt->fetchAll();
    }
    
    public function getUniqueTerms($p = 0) {
		$sql = "SELECT DISTINCT term_id, name FROM v_terms_taxonomy WHERE parent = $p ORDER BY name";
		$stmt = $this->query($sql);
		
		return $stmt->fetchAll();
    }
    
    /**
     * Returns the taxonomies of this term
     */
    public function getTermTaxonomies($txn) {
        $txn = $this->real_escape_string($txn);
	
        $sql = "SELECT DISTINCT taxonomy FROM v_terms_taxonomy WHERE name = '$txn' ORDER BY taxonomy";

        return $this->getSimpleArray($sql);
    }
    
    public function getTerm($idt) {
         $sql = "SELECT * FROM wp_terms WHERE term_id = $idt";
         return $this->getObject($sql);
    }
    
    public function getTermMeta($idt) {
        $sql = "SELECT * FROM wp_termmeta WHERE term_id = $idt ORDER BY meta_key";
        $rs = $this->query($sql);
        $meta = array();

        while($tm = $rs->fetch_object()) {
	        $meta[$tm->meta_key] = $tm->meta_value;
	    }

	    return $meta;
    }

    public function get_cron() {
        $sql = "SELECT option_value FROM wp_options WHERE option_name = 'cron' LIMIT 1";

        $stmt = $this->query($sql);

        $cron_ser = $stmt->fetchColumn(0);

        return unserialize($cron_ser);
    }

    public function clear_cron() {
        $sql = "UPDATE wp_options SET option_value = '' WHERE option_name = 'cron' LIMIT 1";

        $this->query($sql);

        return true;
    }

    public function getTermMetaCSV($idt) {
		$meta = $this->getTermMeta($idt);
		
		$arr = array();
		foreach($meta as $k => $v) {
			$arr[] = "[$k = $v]";
		}
		
		return implode(', ', $arr);
    }

    public function output_row($html, $flds, $array) {
		foreach($flds as $v) {
			
			if (isset($array[$v])) {
				$html = preg_replace('/\$\{' . $v . '\}/i', $array[$v], $html);
			} else {
				$html = preg_replace('/\$\{' . $v . '\}/i', '', $html);
			}
			
		}
		
		echo utf8_encode($html);
    }

    public function getTemplateFields($tmpl) {
		$fields_count = preg_match_all('/\$\{(\w+)\}/i', $tmpl, $fields);

		if ($fields_count) {
			return $fields[1];
		}
		
		return false;
    }

    public function listByTemplate($tmpl_file, $list) {
		$tmpl = getTemplateContent($tmpl_file);

		//echo htmlentities($tmpl);

		if (!$tmpl) trigger_error ('template not found', E_USER_ERROR);

		$use_fields = $this->getTemplateFields($tmpl);

		foreach($list as $row) {			
			$this->output_row($tmpl, $use_fields, $row);
		}
    }
    
    public function listTable($tb) {
		$sql = "SELECT * FROM $tb";
		
		$this->select_to_table($sql);
    }
}

$conn = new MPConn('mysql:host=' . DB_CONN_HOST . ';dbname=' . DB_CONN_DATABASE, DB_CONN_USER, DB_CONN_PASSW);
