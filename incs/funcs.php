<?php

function echo_utf8($str) {
    echo utf8_encode($str);
}

function http_get($p, $def = '') {
    return isset($_GET[$p]) ? $_GET[$p] : $def;
}

function getTemplateContent($tmpl_name) {
    
    $path = "_templates/$tmpl_name.tmpl.html";
    
    if (is_file($path)) {
	return file_get_contents($path);
    } else {
	trigger_error("template [$path] not found", E_USER_ERROR);
    }
}

function replaceData($content, $key, $value) {
    return preg_replace('/\$\{' .$key . '\}/i', $value, $content);
}

function jsonAsHTML($json) {
    
    $j2 = '';
    
    $j2 = $json;
    
    //$j2 = str_replace('{', '{<br>&nbsp;&nbsp;&nbsp;&nbsp;', $j2);
    
    $j2 = print_r(json_decode($json, true), true);
    
    return $j2;
}

function getScriptName() {
    return filter_input(INPUT_SERVER, 'SCRIPT_NAME', FILTER_SANITIZE_STRING);
}

function select_to_table($conn, $sql, $table_attribs = array()) {
	$stmt = $conn->query($sql);
	
	$fields = $rs->fetch_fields();
	
	//print_r($fields);
	
	$tmpl_table = getTemplateContent('table');
	$tmpl_tr = getTemplateContent('tr');
	$tmpl_td = getTemplateContent('td');
	$tmpl_th = getTemplateContent('th');
	
	$table_head_html = "";
	foreach($fields as $f) {
	    $table_head_html .= replaceData($tmpl_th, 'content', $f->name);
	}
	
	$table_head_tr_html = replaceData($tmpl_tr, 'tds', $table_head_html);
	
	$table_html = replaceData($tmpl_table, 'thead', $table_head_tr_html);
	$table_html = replaceData($table_html, 'tfoot', '');
	
	$table_body_html = '';
	while($row = $rs->fetch_assoc()) {
	    $tr = '';
	    foreach($row as $v) {
		$td = replaceData($tmpl_td, 'content', $v);
		$tr .= $td;
	    }
	    $table_body_html .= replaceData($tmpl_tr, 'tds', $tr);
	}
	
	$table_html = replaceData($table_html, 'tbody', $table_body_html);
	
	$atts = arrayToTagAttribs($table_attribs);
	
	if ($atts) {
	    $atts = ' ' . $atts;
	}
	
	$table_html = replaceData($table_html, 'attribs', $atts);
	
	echo $table_html;
    }
    
    function arrayToTagAttribs($attribs, $delim = '"') {
	if (!$attribs) {return '';}

	$list = array();
	foreach($attribs as $k => $v) {
	    $list[] = $k . '=' . $delim . $v . $delim;
	}

	return implode(' ', $list);
    }
    
    function arrayToList($arr) {
    	echo "<ul>";
    	foreach($arr as $k => $v) {
    	
    		echo '<li>' . $k;
    		
    		$v_type = gettype($v);
    		
    		if ($v_type != 'string') {
    			echo " ($v_type)";
    		}

    		echo ' => ';

            if ($v_type == 'array') {
    			arrayToList($v);
    		} elseif ($v_type == 'object') {
    			arrayToList((array) $v);
            } else {
                echo $v;
            }
    		echo '</li>' . PHP_EOL;
    	}
    	echo "</ul>";
    }
    