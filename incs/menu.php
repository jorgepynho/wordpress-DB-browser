<nav class="ink-navigation">
<ul class="menu horizontal blue">
    <li><a href="/"><?php echo WEB_APP_NAME; ?></a></li>
    <li><a href="post_types.php">post_types</a></li>
    <li><a href="terms.php">terms</a></li>
    <li><a href="taxonomies.php">taxonomies</a></li>
    <li><a href="#" onclick="return false;">posts</a>
	<ul class="submenu blue">
	    <li><a href="posts.php">Tree</a></li>
	    <li><a href="posts.php?v=flat">Flat</a></li>
	</ul>
    </li>
    <!--li><a href="posts_types_txns.php">posts types / taxonomies</a></li-->
    <li><a href="term_meta.php">term meta</a></li>
    <li><a href="options.php">options</a></li>
    <li><a href="users.php">users</a></li>
    <li><a href="cron.php">cron</a></li>
    <li class="ink-separator"></li>
    <li class="heading"><a href="<?=$wp_home ?>" target="_blank">FE</a></li>
    <li class="heading"><a href="<?=$wp_siteurl ?>" target="_blank">BO</a></li>
</ul>
</nav>
