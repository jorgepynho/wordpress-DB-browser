<?php
    require "_config.php";

    $page = "post";

    $idp = http_get('idp', 0);

    $post = $conn->getPost($idp);

    if (!$post) {
    	$slug = http_get('slug', '');
    	$type = http_get('type', '');

    	$filter = array();
    	if ($slug) $filter[] = "AND post_name = '" . $slug . "'";
    	if ($type) $filter[] = "AND post_type = '" . $type . "'";

        $rs_posts = $conn->getPosts($filter);

        $page = "posts_list";
    } else {
    	$winTitle = 'Post: ' . $post->ID . ' ' . $post->post_name;
    }

    if (!$post && !$rs_posts) {
        echo("não tem post");
        exit;
    }

    require $template_path;
