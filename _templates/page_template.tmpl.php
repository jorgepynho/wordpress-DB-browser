<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $winTitle; ?> | Wordpress DB viewer</title>
    <link type="text/css" rel="stylesheet" href="//fastly.ink.sapo.pt/2.2.1/css/ink-min.css">
    <link type="text/css" rel="stylesheet" href="/css/styles.css">
</head>
<body>
	<div class="ink-grid">
	<?php require "incs/menu.php"; ?>
	<?php
	    if ($page) {
		require "pages/$page.pg.php";
	    }
	?>
	</div>
	<footer class=""><a href="<?php echo WEB_APP_URL; ?>"><?php echo WEB_APP_NAME; ?></a></footer>
</body>
</html>
