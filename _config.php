<?php
	$server_name = $_SERVER['SERVER_NAME'];

	$page = "";
	$winTitle = '';
	
	$env_file = 'envs/' . $server_name . '.env.inc.php';

	require $env_file;

    require "incs/conn.php";
    require "incs/funcs.php";

    $template_path = '_templates/page_template.tmpl.php';

    $wp_home = $conn->get_wp_home(); // FE
    $wp_siteurl = $conn->get_wp_siteurl(); // BO
